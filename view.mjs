class DOMHelper
{
    // Wrapper for document.createElement to create element in one line with parameters
    createElem(type, params)
    {
        const res = document.createElement(type); // Creating the element

        if (params)
        {
            // Setting the params
            for (let key in params)
            {
                if (key === 'class') res.classList.add(...params[key].split(' '));
                else if (key === 'parent') params[key].appendChild(res);
                else if (key === 'appear') this.animate(res, {anim: key, time: 1000});
                else if (['checked', 'hidden'].includes(key) && params[key]) res.setAttribute(key, '');
                else res[key] = params[key];
            }
        }
        
        return res;
    }

    // Wrapper for document.querySelector
    findElem(query)
    {
        return document.querySelector(query);
    }

    // Wrapper for document.querySelectorAll
    findElems(query)
    {
        return Array.from(document.querySelectorAll(query));
    }

    // Method to add animation to an element by adding and removing classes that are animated in CSS
    animate(elem, params)
    {
        elem.classList.add(params.anim);
        setTimeout(() => {
            if (params.anim === 'disappear') elem.style.display = 'none';
            elem.classList.remove(params.anim);
        }, params.time);
    }
}

const helper = new DOMHelper(); // Creating DOMHelper's object to use it's methods

// Method for initializing the start window
async function initialize()
{
    // Initializing form for entering a name
    const form = helper.createElem('form', {id: 'login-form', class: 'position-center', parent: document.body, appear: true});
    helper.createElem('input', {type: 'text', placeholder: 'Введите ваше имя', id: 'login-field', parent: form});
    helper.createElem('input', {type: 'image', src: 'resources/icons/login-btn.png', id: 'login-btn', parent: form});

    initEvents();
}

// Method for initializing start window events (click on login button)
function initEvents()
{
    helper.findElem('#login-btn').addEventListener('click', (event) => {
        const fieldValue = String(document.querySelector('#login-field').value).trim();

        if (!fieldValue) helper.animate(helper.findElem('#login-form'), {anim: 'shake', time: 200}); // Make 'shake' animation if field is empty
        else openMainWindow(fieldValue);

        event.preventDefault(); // Disabling the standard behavior for this event so that the page is not updated
    });
}

// Method for initializing the main window
function openMainWindow(name)
{
    helper.animate(helper.findElem('#login-form'), {anim: 'disappear', time: 1000}); // Make 'disappear' animation while opening main window to hide login form

    setTimeout(() => {
        document.body.innerHTML = ''; // Clearing the start window

        // Converting the name to the desired format with any user input
        name = name.toLowerCase();
        name = name.replace(name[0], name[0].toUpperCase());

        // Header with user's name
        helper.createElem('header', {id: 'greet', innerText: `Привет, ${name}!`, parent: document.body, appear: true});

        // Form to find a weather in some place
        const form = helper.createElem('form', {id: 'search-form', class: 'position-center', parent: document.body, appear: true});
        helper.createElem('input', {type: 'text', placeholder: 'Введите ваш запрос', id: 'search-field', parent: form});
        helper.createElem('input', {type: 'image', src: 'resources/icons/search-btn.png', id: 'search-btn', parent: form});

        // Checkboxes for filters
        const checkboxes = helper.createElem('form', {id: 'form-checkbox', parent: document.body, appear: true});
        helper.createElem('span', {id: 'filters-title', class: 'filters-text', innerHTML: 'Фильтры', parent: checkboxes});

        // All of the filters
        const filters = [
            ['feels_like', 'Ощущается как'],
            ['wind_dir', 'Направление ветра'],
            ['wind_speed', 'Скорость ветра'],
            ['pressure', 'Давление'],
            ['humidity', 'Влажность'],
            ['season', 'Время года'],
            ['prec_type', 'Тип осадков']
        ];

        // Creating filters
        for (let i of filters)
        {
            const form = helper.createElem('form', {id: `form-${i[0]}`, parent: checkboxes});
            helper.createElem('input', {type: 'checkbox', id: `checkbox-${i[0]}`, class: 'filters', checked: true, parent: form});
            helper.createElem('label', {id: `text-${i[0]}`, class: 'filters-text', innerHTML: i[1], parent: form});
        }

        // Modes of search
        const modes = helper.createElem('form', {id: 'form-modes', parent: document.body, appear: true});
        helper.createElem('span', {id: 'modes-title', class: 'modes-text', innerHTML: 'Режимы', parent: modes});

        const mode_forecast = helper.createElem('form', {id: 'form-forecast', parent: modes});
        helper.createElem('input', {type: 'checkbox', id: 'checkbox-forecast', parent: mode_forecast});
        helper.createElem('label', {id: 'text-forecast', class: 'modes-text', innerHTML: 'Прогнозы', parent: mode_forecast});
        helper.createElem('input', {type: 'text', id: 'field-forecast', placeholder: 'Введите кол-во дней', value: 0, hidden: true, parent: modes});

        const forecasts = helper.createElem('form', {id: 'form-forecasts', parent: document.body}); // Forecasts form

        for (let i = 0; i < 8; i++)
        {
            // Info to create main weather window
            const ind = i === 0 ? '' : i - 1;
            const forecast = i !== 0 ? ' forecast' : '';
            const parent = i !== 0 ? forecasts : document.body;
            const center = i !== 0 ? '' : ' position-center' ;
            const hidden = i !== 0 ? true : false;
            
            const weather = helper.createElem('form', {id: `weather-form${ind}`, class: 'weather' + forecast + center, parent: parent}); // Form with weather info

            // Form with main weather info
            helper.createElem('span', {id: `weather-locality${ind}`, class: 'weather-main weather-info', hidden: hidden, parent: weather});
            helper.createElem('img', {id: `weather-img${ind}`, class: 'weather-main weather-info', hidden: hidden, parent: weather});
            helper.createElem('span', {id: `weather-temp${ind}`, class: 'weather-main weather-info', hidden: hidden,  parent: weather});
            helper.createElem('span', {id: `weather-condition${ind}`, class: 'weather-main weather-info', hidden: hidden,  parent: weather});

            // Additional weather info
            helper.createElem('span', {id: `weather-feels_like${ind}`, class: 'weather-additional weather-info', hidden: hidden, parent: weather});
            helper.createElem('span', {id: `weather-wind_dir${ind}`, class: 'weather-additional weather-info', hidden: hidden, parent: weather});
            helper.createElem('span', {id: `weather-wind_speed${ind}`, class: 'weather-additional weather-info', hidden: hidden, parent: weather});
            helper.createElem('span', {id: `weather-pressure${ind}`, class: 'weather-additional weather-info', hidden: hidden, parent: weather});
            helper.createElem('span', {id: `weather-humidity${ind}`, class: 'weather-additional weather-info', hidden: hidden, parent: weather});
            helper.createElem('span', {id: `weather-season${ind}`, class: 'weather-additional weather-info', hidden: hidden, parent: weather});
            helper.createElem('span', {id: `weather-prec_type${ind}`, class: 'weather-additional weather-info', hidden: hidden, parent: weather});

            helper.createElem('span', {id: `weather-date${ind}`, class: 'weather-main weather-info weather-date', parent: weather}); // Date of weather forecast

            if (i === 0) helper.createElem('span', {id: 'weather-error', parent: weather}); // Error message
            if (i === 1) weather.setAttribute('forecast-active', '');
        }
        
        initMainEvents();
    }, 1000);
}

// Method for initializing main window events (click on search button)
function initMainEvents()
{
    helper.findElem('#search-btn').addEventListener('click', async (event) => {
        const fieldValue = String(helper.findElem('#search-field').value).toLowerCase().trim();
        const forecastValue = Number(helper.findElem('#field-forecast').value);

        event.preventDefault();

        if (!fieldValue || document.body.hasAttribute('search-btn-pressed')) helper.animate(helper.findElem('#search-form'), {anim: 'shake', time: 200});
        else
        {
            const filters = {}; // Filters object for getWeather function
            helper.findElems('.filters').forEach((elem) => {
                filters[elem.id.split('-')[1]] = elem.checked; // Filling in filters object
                elem.disabled = !elem.checked; // Disabling unchecked checkboxes
            });

            const weather_info = await (await import('./transport.mjs')).getWeather(fieldValue, filters, forecastValue); // Getting weather data

            const weather = helper.findElem('#weather-form');
            weather.style.display = 'grid';
            helper.animate(weather, {anim: 'appear', time: 1000}); // Make 'appear' animation at weather form

            // Something went wrong
            if (weather_info.error)
            {
                helper.findElems('.weather-info').forEach((elem) => {
                    if (elem.id !== 'weather-img') elem.innerHTML = '';
                });

                helper.findElem('#weather-error').innerHTML = weather_info.error; // Error message
                helper.findElem('#weather-img').src = 'resources/icons/error.svg'; // Error icon
                return;
            }

            // Filling in the main weather data
            helper.findElems('.weather-main').forEach((elem) => {
                let name = elem.id.split('-')[1];
                const ind = isNaN(Number(name[name.length - 1])) ? 0 : Number(name[name.length - 1]);
                name = name.replace(/[0-9]/g, '');
                const attr = name === 'img' ? 'src' : 'innerHTML'; 
                elem[attr] = weather_info[ind] ? weather_info[ind][name].value : '';
            });

            // Filling in the additional weather data
            helper.findElems('.weather-additional').forEach((elem) => {
                let name = elem.id.split('-')[1];
                let oldName = name;
                const ind = isNaN(Number(name[name.length - 1])) ? 0 : Number(name[name.length - 1]);
                name = name.replace(/[0-9]/g, '');
                elem.innerHTML = weather_info[ind] ? (weather_info[ind][name] ? `${weather_info[ind][name].desc}: ${weather_info[ind][name].value}` : '') : '';
                if (isNaN(oldName[oldName.length - 1])) elem.hidden = false;
            });

            // Adding forecasts if they are active
            for (let i = 0; i < forecastValue + 1; i++)
            {
                let elem = helper.findElem(`#weather-form${i}`);
                elem.style.display = 'flex';
                helper.animate(elem, {anim: 'appear', time: 200});
                elem.removeAttribute('forecast-active');
                if (i === 0) elem.setAttribute('forecast-active', '');
            }

            helper.findElem('#weather-error').innerHTML = '';
        }

        document.body.setAttribute('search-btn-pressed', ''); // Atribute to indicate if search button was pressed
        event.preventDefault();
    });

    // If the request has been changed, we allow user to click search button again
    helper.findElem('#search-field').addEventListener('input', () => {
        document.body.removeAttribute('search-btn-pressed');
        changeCheckboxes(true); // Enabling all checkboxes to change filters with new request
    });

    // Hiding weather form if 'Escape' was pressed
    window.addEventListener('keydown', (event) => {
        let elem = document.querySelector('#weather-form'); 
        if (event.key === 'Escape' && elem)
        {
            helper.animate(elem, {anim: 'disappear', time: 1000});

            helper.findElem('.weather.forecast[forecast-active]').removeAttribute('forecast-active');
        }
    });

    // Events to filter
    helper.findElem('#form-checkbox').addEventListener('change', (event) => {
        if (document.body.hasAttribute('search-btn-pressed'))
        {
            const name = event.target.id.split('-')[1]; // Getting name of the checkbox
            helper.findElem(`#weather-${name}`).hidden = !event.target.checked; // Hiding or showing corresponding element of weather data
        }
    });

    // Forecast mode checkbox
    helper.findElem('#checkbox-forecast').addEventListener('change', (event) => {
        document.body.removeAttribute('search-btn-pressed');
        changeCheckboxes(true);

        const elem = helper.findElem('#field-forecast');
        elem.hidden = !event.target.checked;
        elem.value = event.target.checked ? 1 : 0;
    });

    // Correct input in forecast field
    helper.findElem('#field-forecast').addEventListener('input', (event) => {
        document.body.removeAttribute('search-btn-pressed');
        changeCheckboxes(true);

        if (!isNaN(Number(event.data)) && Number(event.data) > 0 && Number(event.data) < 7) event.target.value = event.data;
        else event.target.value = 1;
    });

    // Visualize some forecast
    helper.findElem('#form-forecasts').addEventListener('click', (event) => {
        const name = event.target.id.split('-')[1];
        const ind = Number(name[name.length - 1]);
        const weather_form = helper.findElem('#weather-form'); // Main weather form
        const elem = helper.findElem(`#weather-form${ind}`); // Forecast that was clicked

        if (event.target.id === 'form-forecasts' || elem.hasAttribute('forecast-active') || document.body.hasAttribute('forecast-working')) return;
        else
        {
            document.body.setAttribute('forecast-working', ''); // Prevent the possibility of multiple clicks by adding forecast-working attribute

            if (helper.findElem('.weather.forecast[forecast-active]')) helper.findElem('.weather.forecast[forecast-active]').removeAttribute('forecast-active'); // Removing the attribute from the old forecast

            elem.setAttribute('forecast-active', ''); // Setting the attribute to new forecast

            helper.animate(weather_form, {anim: 'disappear', time: 1000});

            setTimeout(() => {
                Array.from(elem.children).forEach((child) => {
                    let child_name = child.id.split('-')[1];
                    child_name = child_name.replace(/[0-9]/g, '');
                    const attr = child_name === 'img' ? 'src' : 'innerHTML';
                    helper.findElem(`#weather-form > #weather-${child_name}`)[attr] = child[attr]; // Copying data from forecast to main weather form
                });

                weather_form.style.display = 'grid';
                helper.animate(weather_form, {anim: 'appear', time: 1000});

                document.body.removeAttribute('forecast-working');
            }, 1000);
        }
    });
}

// Method to enable or disable all checkboxes in filters
function changeCheckboxes(condition)
{
    helper.findElems('.filters').forEach((elem) => {
        elem.disabled = !condition;
    });
}

window.addEventListener('load', () => {
    initialize();
});