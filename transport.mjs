// Method to get weather data in some place
export async function getWeather(place, filters, forecast)
{
    // Import dictionaries to translate some weather data, importing api keys
    const {condition_ru, wind_dir_ru, season_ru, prec_type_ru, prec_strength_ru} = (await import('./dicts.mjs')).default;
    const {geocoder_key, weather_key} = (await import('./api_keys.mjs')).api_keys;

    // Geocoder request and response to get place's coordinates by name
    const geocoder_request = `https://geocode-maps.yandex.ru/1.x/?apikey=${geocoder_key}&geocode=${place}&format=json&lang=ru_RU`;
    const geocoder_response = await fetch(geocoder_request, {method: 'GET'});

    if (!geocoder_response.ok) return [{error: 'Неизвестная ошибка'}]; // Checking that the request was executed successfully

    const geocoder_data = await geocoder_response.json(); // Getting geocoder data as JSON
    
    if (geocoder_data.response.GeoObjectCollection.featureMember.length === 0) return {error: 'По вашему запросу ничего не найдено'}; // Checking that something was found on the request

    // Getting response coordinates, converting coordinates to an object
    const resp_coords = geocoder_data.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos.split(' ');
    const coords = {lon: resp_coords[0], lat: resp_coords[1]};
    
    // Weather API request to get all of the weather info
    const weather_request = `https://api.weather.yandex.ru/v2/forecast?lat=${coords.lat}&lon=${coords.lon}`;
    const weather_response = await fetch(weather_request, {method: 'GET', headers: {
      'X-Yandex-API-Key': weather_key
    }});

    if (!weather_response.ok) return [{error: 'Неизвестная ошибка'}]; 

    const weather_data = await weather_response.json(); 

    // Initialization of necessary weather data
    let weather_arr = [];
    let source = weather_data.fact;
    const locality = {value: weather_data.geo_object.locality ? weather_data.geo_object.locality.name : weather_data.geo_object.country.name, desc: 'Местонахождение'};
    const season = filters.season ? {value: season_ru[source.season], desc: 'Время года'} : null;

    // Converting data to the desired format
    for (let i = -1; i < forecast; i++)
    {
      if (i !== -1) 
      {
        weather_data.forecasts[i + 1].parts.day_short.date = weather_data.forecasts[i + 1].date; // Moving the date to the desired object
        source = weather_data.forecasts[i + 1].parts.day_short; // Changing source to get forecasts
      }

      const date = source.date ? source.date : new Date().toISOString().split('T')[0]; // If there is no date in the source, we take the current date

      // Making object containing weather info
      const weather_info = {
        locality: locality,
        img: {value: `https://yastatic.net/weather/i/icons/funky/dark/${source.icon}.svg`, desc: 'Иконка погоды'},
        condition: {value: condition_ru[source.condition], desc: 'Описание погоды'},
        temp: {value: `${source.temp} &deg;C`, desc: 'Температура'},
        feels_like: filters.feels_like ? {value: `${source.feels_like} &deg;C`, desc: 'Ощущается как'} : null,
        wind_dir: filters.wind_dir ? {value: wind_dir_ru[source.wind_dir], desc: 'Направление ветра'} : null,
        wind_speed: filters.wind_speed ? {value: `${source.wind_speed} м/с`, desc: 'Скорость ветра'} : null,
        pressure: filters.pressure ? {value: `${source.pressure_mm} мм. рт. ст.`, desc: 'Давление'} : null,
        humidity: filters.humidity ? {value: `${source.humidity}%`, desc: 'Влажность'} : null,
        season: season,
        prec_type: filters.prec_type ? {value: prec_strength_ru[source.prec_strength] + prec_type_ru[source.prec_type], desc: 'Тип осадков'} : null,
        date: {value: date, desc: 'Дата'}
      };

      weather_arr.push(weather_info); // Put it in the result array
    }

    return weather_arr;
}