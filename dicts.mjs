// Dictionaries for correct translation of data into Russian, as well as the conversion of numeric symbols into text
const dicts = ( () => {
    const condition = {
        clear: 'Ясно',
        'partly-cloudy': 'Малооблачно',
        cloudy: 'Облачно с прояснениями',
        overcast: 'Пасмурно',
        drizzle: 'Морось',
        'light-rain': 'Небольшой дождь',
        rain: 'Дождь',
        'moderate-rain': 'Умеренно сильный дождь',
        'heavy-rain': 'Сильный дождь',
        'continuous-heavy-rain': 'Длительный сильный дождь',
        showers: 'Ливень',
        'wet-snow': 'Дождь со снегом',
        'light-snow': 'Небольшой снег',
        snow: 'Снег',
        'snow-showers': 'Снегопад',
        hail: 'Град',
        thunderstorm: 'Гроза',
        'thunderstorm-with-rain': 'Дождь с грозой',
        'thunderstorm-with-hail': 'Гроза с градом',
      };
      
      const wind_dir = {
        nw: 'северо-западное',
        n: 'северное',
        ne: 'северо-восточное',
        e: 'восточное',
        se: 'юго-восточное',
        s: 'южное',
        sw: 'юго-западное',
        w: 'западное',
        c: 'штиль',
      };
      
      const season = {
        summer: 'лето',
        autumn: 'осень',
        winter: 'зима',
        spring: 'весна'
      };
      
      const prec_type = {   
        0: 'без осадков',
        1: 'дождь',
        2: 'дождь со снегом',
        3: 'снег',
        4: 'град',
      };
      
      const prec_strength = {
        0: '',
        0.25: 'слабый ',
        0.5: '',
        0.75: 'сильный ',
        1: 'очень сильный ',
      };

    return {
        condition_ru: condition,
        wind_dir_ru: wind_dir,
        season_ru: season,
        prec_type_ru: prec_type,
        prec_strength_ru: prec_strength
    }
})();

export default dicts;

// Using closure